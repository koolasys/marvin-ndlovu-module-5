import 'package:flutter/material.dart';
import 'package:marvin_ndlovu_module_5/widgets/profile.dart';

const duration = Duration(milliseconds: 1000);

Route toProfile() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) =>
        const ProfilePage(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(0, 1);
      const end = Offset(0, 0);
      const curve = Curves.decelerate;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
    transitionDuration: duration,
  );
}

// Route toDashboard() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => const MainPage(),
//     transitionsBuilder: (context, animation, secondaryAnimation, child) {
//       const begin = Offset(0, -1);
//       const end = Offset(0, 0);
//       const curve = Curves.decelerate;

//       var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

//       return SlideTransition(
//         position: animation.drive(tween),
//         child: child,
//       );
//     },
//     transitionDuration: const Duration(seconds: 4),
//   );
// }

// Route toLogin() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => const SignInPage(),
//     transitionsBuilder: (context, animation, secondaryAnimation, child) {
//       const begin = Offset(-1, 0);
//       const end = Offset(0, 0);
//       const curve = Curves.decelerate;

//       var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

//       return SlideTransition(
//         position: animation.drive(tween),
//         child: child,
//       );
//     },
//     transitionDuration: duration,
//   );
// }

// Route toSignup() {
//   return PageRouteBuilder(
//     pageBuilder: (context, animation, secondaryAnimation) => const SignUpPage(),
//     transitionsBuilder: (context, animation, secondaryAnimation, child) {
//       const begin = Offset(1, 0);
//       const end = Offset(0, 0);
//       const curve = Curves.easeInToLinear;

//       var tween = Tween(
//         begin: begin,
//         end: end,
//       ).chain(CurveTween(curve: curve));

//       return SlideTransition(
//         position: animation.drive(tween),
//         child: child,
//       );
//     },
//     transitionDuration: duration,
//   );
// }
