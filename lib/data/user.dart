class User {
  final String imgPath;
  var name;
  final String surname;
  final String email;
  final String about;

  User(
      {required this.imgPath,
      required this.name,
      required this.surname,
      required this.email,
      required this.about});
}
