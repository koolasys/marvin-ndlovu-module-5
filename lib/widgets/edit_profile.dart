import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:marvin_ndlovu_module_5/routes/routes.dart';
import 'package:marvin_ndlovu_module_5/widgets/profile.dart';
import '../data/user.dart';
import '../data/user_data.dart';
import '../themes/theme.dart';
import 'profile_widget.dart';
import './profile_edit_widgets.dart';

class EditProfile extends StatefulWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  State<EditProfile> createState() => _EditProfileState();
}

User user = UserPreferences.myUser;

class _EditProfileState extends State<EditProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: ListView(
        padding: const EdgeInsets.symmetric(horizontal: 23),
        physics: const BouncingScrollPhysics(),
        children: [
          const SizedBox(height: 15),
          ProfileWidget(
            imgPath: user.imgPath,
            onClicked: () async {},
            isEdit: true,
          ),
          ProfileTextInput(
            label: "Full Name",
            text: user.name + " " + user.surname,
            onChanged: (name) {},
            maxLines: 1,
          ),
          ProfileTextInput(
            label: "Email",
            text: user.email,
            onChanged: (name) {},
            maxLines: 1,
          ),
          ProfileTextInput(
            label: "About Me",
            text: user.about,
            onChanged: (name) {},
            maxLines: 6,
          ),
          saveEditButton(context),
          buildDeleteProfile(context),
        ],
      ),
    );
  }
}

AppBar buildAppBar(BuildContext context) {
  return AppBar(
    title: const Text("Edit Profile"),
    leading: const BackButton(),
    backgroundColor: Colors.transparent,
    elevation: 0,
  );
}

/// Add profile function
void addProfile() {
  FirebaseFirestore.instance.collection('user-pref').doc("user-data").set({
    'name': user.name,
    'surname': user.surname,
    'email': user.email,
    'about': user.about
  });
}

/// Delete profile function
void deleteProfile() {
  FirebaseFirestore.instance.collection("user-pref").doc("user-data").delete();
}

///Save profile button widget
Widget saveEditButton(BuildContext context) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 35),
    width: 300,
    child: ElevatedButton(
        onPressed: addProfile,
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Text(
            "Save Profile",
            style: Theme.of(context).textTheme.headline3,
          ),
        ),
        style: buttonStyle //
        ),
  );
}

///Delete profile button widget
Widget buildDeleteProfile(BuildContext context) {
  return Container(
    padding: const EdgeInsets.symmetric(vertical: 35),
    width: 300,
    child: const TextButton(
      onPressed: deleteProfile,
      child: Padding(
        padding: EdgeInsets.all(15),
        child: Text(
          "Delete Profile",
          style: TextStyle(
            color: Colors.red,
          ),
        ),
      ),
      //style: buttonStyle //
    ),
  );
}
